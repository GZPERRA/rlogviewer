# rLogViewer
## _To Visualize The Logs_

rLogViewer is a web based interface for viewing and analyzing rsyslog logs.

- Run one command.
- Open localhost:7000
- ✨Magic ✨

## Installation [Server]
The following command will take care of the hole installation process. It downloads the server setup script and runs it as root. If you're familiar with bash script, take a look at it [here](https://gitlab.com/GZPERRA/rlogviewer/-/blob/main/installer/setup.sh).

```sh
sudo su -c "bash <(wget -qO- https://gitlab.com/GZPERRA/rlogviewer/-/raw/main/installer/setup.sh)" root
```

## License

MIT

**Free Software!**


